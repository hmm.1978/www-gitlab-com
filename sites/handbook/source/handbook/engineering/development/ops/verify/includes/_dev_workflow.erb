We use the [<%= group %> Workflow issue board](https://gitlab.com/groups/gitlab-org/-/boards/<%= board_id %>?milestone_title=%23upcoming) to track what we work on in the current milestone.

Development moves through workflow states in the following order:

 1. `workflow::design` (if applicable)
 1. `workflow::planning breakdown` 
 1. `workflow::ready for development`
 1. `workflow::in dev`
 1. `workflow::blocked` (as necessary)
 1. `workflow::in review`
 1. `workflow::verification`
 1. `workflow::production`
 1. `Closed`

`workflow::planning breakdown` is driven by Product, but is a collaborative effort between Product, UX and Engineering. The steps for **planning breakdown** typically consists of:
 - Product defining or clarifying the problem statement. Product and UX will collaborate on `problem validation` as needed
 - UX providing designs (as necessary)
 - Engineering clarifying the issue description as stated, and refines and weights the issue once Product and UX have provided enough details to do so.

At any point, if an issue becomes blocked, it would be in the `workflow::blocked` status. If there is a blocking issue, it needs to be added to the issue description or linked to the issue with a 'blocked by' relationship.

`workflow::ready for development` means that an issue has been sufficiently [refined and weighted by Engineering](##how-engineering-refines-issues), upon request by Product and UX

`Closed` means that all code changes associated with the issue are fully enabled on gitlab.com. If it is being rolled out behind a feature flag, it means the feature flag is enabled for all users on gitlab.com.

#### "What do I work on next?"

Each member of the team can choose which issues to work on during a milestone by assigning the issue to themselves.  When the milestone is well underway and we find ourselves looking for work, we default to working **right to left** on the **issue board** by pulling issues in the right-most column. If there is an issue that a team member can help with on the board, they should do so instead of starting new work. This includes conducting code review on issues that the team member may not be assigned to, if they feel that they can add value and help move the issue along to completion.

Specifically, this means our work is prioritized in the following order:
 * Any verification on code that is in `~workflow::verification` or `workflow::production`
 * Conducting code reviews on issues that are `workflow::in review`
 * Unblocking anyone in `workflow::blocked` or `workflow::in dev` if applicable
 * Then, lastly, picking from the top of the `workflow::ready for development` for development column

The goal of this process is to reduce the amount of work in progress (WIP) at any given time. Reducing WIP forces us to "Start less, finish more", and it also reduces cycle time. Engineers should keep in mind that the DRI for a merge request is **the author(s)**, to reflect the importance of teamwork without diluting the notion that having a [DRI is encouraged by our values](/handbook/people-group/directly-responsible-individuals/#dris-and-our-values).

#### Weekly Issue Progress Updates

In order to keep our stakeholders informed of work in progress, we provide updates to issues either by updating the issue's **health status** and/or adding an **async issue update**.

##### Issue Health Status

For issues in the current milestone, we use the [Issue Health Status feature](https://docs.gitlab.com/ee/user/project/issues/#health-status) to indicate probability that an issue will ship in the current milestone. This status is updated by the DRI ([directly responsible individual](/handbook/people-group/directly-responsible-individuals/)) as soon as they recognize the probability has changed. If there is no change to the status, a comment to indicate that it has been the status of the issue has been _assessed_ would be helpful. 

The following are definitions of the health status options:

- `On Track` - The issue has no current blockers, and is likely to be completed in the current milestone.
- `Needs Attention` - The issue is still likely to be completed in the current milestone, but there are setbacks or time constraints that could cause the issue to miss the release due dates.
- `At Risk` - The issue is highly **unlikely** to be completed in the current milestone, and will probably miss the release due dates.

Examples of how status updates are added:
1. If the health status changes from `On Track` to `Needs attention` or `At Risk`, we recommend that the DRI add a short comment stating the reason for the change in an [issue status update](#issue-status-updates).
1. If an issue continues to be `On Track`, the DRI could provide a comment to indicate solutions (whatever it may be) continue to be implemented, and it's still _on track_ to be delivered in the same milestone.

##### Issue status updates

When the DRI is actively working on an issue  (workflow status is `~workflow::in dev`, `~workflow::in review` or `~workflow::verification` in the current milestone), they will add a comment into the issue with a status update, detailing:
- the updated issue health status
- notes on what was done based on the updated issue health status (especially if not `On Track`)
- anything else the DRI feels is beneficial to reflect the progress

There are several benefits to this approach:

- Team members can better identify what they can do to help the issue move along the board
- Creates an opening for other team members to engage and collaborate if they have ideas
- Leaving a status update is a good prompt to ask questions and start a discussion
- The wider GitLab community can more easily follow along with product development
- A history of the roadblocks the issue encountered is readily available in case of retrospection
- Product and Engineering Managers are more easily able to keep informed of the progress of work

Expectations for DRIs when providing updates for work in progress:

- Status updates are provided once per week, barring special circumstances (e.g. PTO)
- Ideally updates are made at a logical part of a DRI's workflow to minimize disruption, and not necessarily at the same time/day each week
  - Generally when there is a logical time to leave an update, such as a _change in issue health status_, that is the best time to do so
  - Can be used to present some technical findings or information relevant to other stakeholders

##### Tracking Inactive Issues

As a general rule, any issues being actively worked on have one of the following workflow labels:

- `workflow::in dev`
- `workflow::in review`
- `workflow::verification`
- `workflow::production` (upon **closing** the issue)

The Health Status of these issues should be updated to:

1. `Needs Attention` - on the `1st` of the month.
1. `At Risk` - on the `8th` of the month.

EMs are responsible for manually updating the Health Status of [any inactive issues in the milestone](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=group%3A%3A<%= group.downcase %>&milestone_title=%23upcoming&not%5Blabel_name%5D%5B%5D=workflow%3A%3Acanary&not%5Blabel_name%5D%5B%5D=workflow%3A%3Ain+dev&not%5Blabel_name%5D%5B%5D=workflow%3A%3Ain+review&not%5Blabel_name%5D%5B%5D=workflow%3A%3Aproduction&not%5Blabel_name%5D%5B%5D=workflow%3A%3Astaging&not%5Blabel_name%5D%5B%5D=workflow%3A%3Averification&page=2&scope=all&state=opened) accordingly.

