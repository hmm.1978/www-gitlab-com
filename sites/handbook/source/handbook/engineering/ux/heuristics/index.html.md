---
layout: handbook-page-toc
title: "UX Heuristics"
description: "Heuristics used by the UX team to evaluate our product."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Intro
UX Heuristics are guidelines we can use to grade experiences in our product. They can also be useful while designing because you can use them to make design decisions or identify parts of your design that still need work.

This list can change and the handbook version is the SSOT. It's based on best practices in the UX field, as well as feedback from our customers as to what they want their GitLab experience to be like.

## Templates
[Competitor Evaluation](https://docs.google.com/spreadsheets/d/1rtJV4a3IW-2jpq4Ifz3GBKcT3xikmZRecQVeBGJ3yz8/edit#gid=0)
UX Scorecard

## Heuristics
**updated May 7, 2021**

| Category | General Heuristics | Description | 
| ------ | ------ | ------ |
| Usability | Visibility of system status | Communicating the current state allows users to feel in control of the system, take appropriate actions to reach their goal, and ultimately trust the brand. (source: NN/g) |
| Usability | Flexibility of use | Shortcuts (unseen by the novice user) speed up the interaction for the experts such that the system can cater to inexperienced and experienced users. (source: NN/g) |
| Usability | User control and freedom | Allow people to exit a flow or undo their last action and go back to the system’s previous state. (source: NN/g) |
| Usability | Recognize, diagnose, recover from errors | Make error messages visible, reduce the work required to fix the problem, and educate users along the way. (source: NN/g) |
| Laws and tenets | Protective: I don’t lose my data | (source: uitraps.com) |
| Laws and tenets | Forgiving: I can undo my actions | (source: uitraps.com) |
| Laws and tenets | Understandable: I know what I can do | (source: uitraps.com) |
| Laws and tenets | Efficient: I take fewer steps and process less information | (source: uitraps.com) |
| Characteristics | Real-time user interface | (see: The future of MRs: Real-time collaboration) |
| Characteristics | A tool the whole team can use | (see: GitLab Direction #personas) |
| Characteristics | Minimal setup required | (see: GitLab Product Principles #convention-over-configuration) |
| Characteristics | For technical areas, documentation is easy to use | cell |
| Onboarding | Learnability and support for new users | cell |


## References
[Nielssen Norman Group](https://www.nngroup.com/articles/ten-usability-heuristics/)

