---
layout: markdown_page
title: "Sanmi Ayotunde Ajanaku's README"
description: "Learn more about working with Sanmi Ayotunde"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Sanmi (pronounced "Sun-Me") Ayotunde Ajanaku's README

Hi there! I'm **Sanmi**, pronounced as **"Sun :sunny: Me"**, which means "Favored by God".  I am a Frontend Engineer and you will love working with me.

- [GitLab Handle](https://gitlab.com/sanmiayotunde)
- [Team Page](https://about.gitlab.com/company/team/#sanmiayotunde)
- [My Website](https://www.dabible.com/)
- [Twitter](https://twitter.com/sanmiayotunde)
- [Facebook](https://www.facebook.com/sanmiayotunde)
- [LinkedIn](https://www.linkedin.com/in/sanmiajanaku/)

## Growing up...

I was born and raised in Osun State, Nigeria, West Africa. As a very young boy, even though I grew up in the village and the local streets of Ilesa, I had many fantastic dreams. I was one of the few who luckily got introduced to computers at a very tender age in the late 90s. Around 2002, my Dad bought a computer for his printing press company. He employed a graphics designer, but to my dad and everyone's shock, I replace her in just a few months because I have been spending nights and day hacking my way into the computer without them noticing.

* I grew up very poor in Africa, even though I never knew we were poor until I got to high school, but I had a lot of huge dreams. Here are a few...

	* Growing up, only rich folks can afford **milk**, one of my childhood dreams was to buy a tin of milk when I grow up and drink everything alone!
	* No one in our family had a car, so I had a used motorcycle tire as a car, and another **imaginary car** that I drive in my mind. Today, I have bought many cars.
	* **Airplanes**, I told my cousin I will be in one someday. He laughed like crazy and never believed me.
	* I will **build a university** in my village - I still have no clue how this will happen, but it will definitely happen.

![Sanmi-Childhood-Dream](https://gitlab.com/gitlab-com/www-gitlab-com/uploads/411423ae1c75055a2417f8d781cbc358/Sanmi-Childhood-Dream.jpg)

I started my first IT company in high school, and this developed my strange passion for computers and self-learning. I have spent all my life self-learning, even though I have a Bachelors's and masters in Computer Science, but my quest for knowledge and programming is unsatiable.

## My Career

I've spent my career in and around web development, mobile app development, startups, advertising, and customer journey transformation. Having worked with startups, B2B, B2C companies, including top Fortune 500s. I have developed a taste of understanding what users want online and how to give it to them easily and quickly.

![image](https://gitlab.com/gitlab-com/www-gitlab-com/uploads/24ce866916d4f73e5cd7cb4e01798ad1/image.png)

## My current role and tools?

- I develop full-stack web applications and re-usable frontend components. I work along with the most talented UI/UX Designers to create web and mobile applications.
- I contribute to stakeholders' decision-making to enhance product development and iterate product until we have a market-fit-product.

 
## About me

* I was born, raised, and studied in Nigeria, West Africa. I moved to the United States in 2013 after my Bachelor's Degree.
* In Nigeria, I was a performing musician until I had a turn around to become a Christian.
* In the United States, I have lived in Spokane. - Washington States,  Atlanta. - Georga, and Huntsville - Alabama. I have visited a lot of these places and approximately 30 states of the United States.
* I am committed to a lot of life-transforming activities for kids, young people, and others going through challenging faces in life.
* I currently have an NGO called [DaBible Foundation](https://www.dabible.com) where I provide mobile apps and solar-powered physical devices that people in Africa can use to listen to the Bible.
* I am a very interesting personality, I am still evolving, and I will be updating my life's operational system everyday lol.

![DaBible-Header-Apple](https://gitlab.com/gitlab-com/www-gitlab-com/uploads/59de4a679313188bce535937b31c9e6c/DaBible-Header-Apple.jpg)

## How I communicate

* **SILLY QUESTIONS** - I am not scared of asking questions, even if it sounds silly. My silly questions light up my imagination and help me to understand your request better.
* **THE BIG PICTURE** - I always love seeing the **big picture**, tell me the end goal, then tell me what you want us to do.
* **DOCUMENTATION!** Documentation!! Documentation!!! - Please don't assume that I understand you. **A simple PowerPoint or Bullet Points** is worth more than a thousand calls lol.
* I always have a lot of tasks on my hand at the same time. I may forget what you told me on the phone or zoom, that's why I prefer proper comment through **issues or slack** 

## My working style

 - **Slack, Slack Slack***  I rarely check emails, if either of us has committed to doing something, let's use slack to communicate clearly about expectations.
 - **I work with creative issues**, Therefore, work doesn't exist to me without it being in an issue, assigned to a milestone. 
 - **I manage my to-do's for quick asynchronous exchanges**. I clean to-do's out every week, and I am usually in them multiple times a day. 
 
I will be updating this page more and more.

Thanks!
