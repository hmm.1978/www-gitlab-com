---
layout: markdown_page
title: "Chris Cowell’s README"
---

I’m **Chris Cowell**, a **Senior Technical Instructor** for **Professional Services**.

This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before. It’s also an effort at building trust by being intentionally vulnerable.

Feel free to contribute to this page by opening a merge request.


## About me

## 2-sentence bio

I was a developer and test automator for 20 years, working for tech behemoths (Accenture, Oracle), smaller pre-IPO startups (Puppet, Jive) and a health insurance company (Cambia/HealthSparq). Since best part of these jobs was when I got to train people, I eventually changed gears to become a technical instructor.


### Favorite GitLab value

**Iteration:** don’t design, build, or test, the whole thing. Design, build, or test a tiny slice of the thing. Seriously: an embarrassingly tiny slice. Then improve on that. I can’t count the number of times I’ve seen companies get into trouble by ignoring this principle. Come to think of it, this is a good principle for most aspects of life.


### Some things that changed my life

+ George Berkeley’s _Three Dialogues between Hylas and Philonous_. Berkeley was a weird philosopher who believed that no physical objects exist -- only ideas. This book argued that point of view so convincingly that it blew my head open. He’s wrong, of course, but this led to a decade-long detour into academic philosophy in my 20s.
+ Dale Carnegie’s _How to Win Friends and Influence People_. Don’t be put off by the sleazy-sounding title! I didn’t know how to talk to people until I read this 1936 book. As it turns out, the more people I talk to, the more interesting they become.
+ National Geographic’s _Atlas of the World_. For much of my childhood I was overseas, where there wasn’t much to do. To pass the time I scrutinized the atlas, and I learned about geography from the maps, design from the flags, and culture from the short writeups of each country. This spurred a love of travel that’s still going strong, if slowed by the constraints of family and work.
+ The Apple II+: unspeakably weak and crude by today’s standards, this magic box led to my college major and career.
+ 4 albums that shaped my tastes in classical, rock, jazz, and world music:
    + Vivaldi: _Gloria_ (RV 589)
    + The Beatles: _Sgt. Pepper’s Lonely Hearts Club Band_
    + Dave Brubeck Quartet: _Take Five_
    + Keola & Kapono Beamer: _Honolulu City Lights_


### Trivia you can ask me about

+ I have 4 teenaged kids or stepkids. They include an albino, an autistic person, someone who runs around the block several times a day with bare feet no matter the weather, and someone who loves geography so much that he told his pediatrician (accurately) that he had a bruise the shape of Nicaragua.
+ I met my wife over an argument about the Oxford comma.
+ I earned a philosophy Ph.D. that I never use. That’s an exaggeration: it hugely influenced how I communicate, teach, and think. But I never use the philosophy part.
+ My picture has been on the _New York Times_ obituaries page not once but twice.
+ Singapore is my favorite country.
+ My most fun job was as a traveling researcher/writer for _Let's Go_ travel guidebooks. Remember travel guidebooks? Yeah, didn't think so.
+ I’m a non-drinking vegetarian. I’d like to wean myself from caffeine eventually, but these days I drink a lot of tea.


## Communicating with me

+ **Put important stuff in writing.** Seriously: as far as my brain is concerned, _if you or I didn’t write it down, it never happened_. There are 2 reasons:
    + My memory is so bad that I sometimes wonder if it’s a borderline disability. No joke.
    + I can have trouble processing spoken words in real time, especially with unfamiliar topics. It’s an odd brain limitation that runs in my family.
+ **Help me understand whether to do a task THOROUGHLY or QUICKLY.** My natural mode is to do things slowly, carefully, and completely. But that’s not always the right approach. If getting your task done soon is more important than getting it done thoroughly, tell me that explicitly and I’ll adapt.
+ **Expect lots of questions.** This probably comes from my background in QA, where it’s critical to understand exactly how things work.
+ When I ask a question, **do your best to answer exactly that question** and not a different, related question. I’ll try to do the same.
+ **Sometimes I talk in bullet points.** This can come across as stilted or weirdly formal, but it’s how my brain keeps ideas in order. I’m trying to notice when I do this and do it less, because it can be irritating to others.
+ **Give me time to context switch.** I’m hopeless at multitasking, and it can take a minute or two to reorient my brain when I switch audiences or topics.
+ **I want to hear your ideas** no matter how insecure you are about their validity or how new you are to your role. You’d be surprised how often something is obvious to you but not to others, and those ideas are lost if you don’t express them.


## How you can help me

+ When in doubt, **over-explain**. I don’t mind if you explain something I already know; I’ll stop you if I already understand. It’s exhausting to hear an explanation with 9 terms I’ve never heard of, and then have to ask what each term means. And when an explanation goes over my head I don’t always know what clarifying questions to ask.
+ **People are more interesting than computers, and I want to know more about you.** Unless I’m under *do-the-thing-right-now* time pressure, I want to hear about your problems getting your kid to eat vegetables, the weird documentary you saw about Belgian monks, or your parent with dementia (I have one too). This goes double if you’re not American: I love learning about other parts of the world.


## Related pages

<!-- TO ITERATE: add links to personal blog posts -->

I ran a 1-person technical training company for a year or so, and have 3 longish posts for tech newbies on the [company’s blog](https://www.christophercowell.llc/blog.html).


